package com.emerio.bootcamp.customerservice.repo;

import java.sql.Timestamp;
import java.util.Optional;

import com.emerio.bootcamp.customerservice.entity.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long>{
    Optional<Customer> findByUsername(String username);
}