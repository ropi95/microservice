package com.emerio.bootcamp.customerservice;

import com.emerio.bootcamp.customerservice.repo.CustomerRepository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerServiceApplication.class, args);
	}

	@Bean
	CommandLineRunner init(CustomerRepository customerRepository){
		return (evt) -> Arrays.asList(
		"ropi,haris,idam,rian,gian".split(","))
		.foreach(
			a -> {
				@SuppressWarnings("unused")
				Customer customer = customerRepository.save(new Customer(a, a, "", a + "@emeriocorp.com","alamat rumah " + a,
				new Timestamp(System.currentTimeMillis())));
			});
	}
}
